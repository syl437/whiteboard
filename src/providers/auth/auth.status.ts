export enum AuthStatuses {
    NOT_AUTHENTICATED,
    AUTHENTICATED
}