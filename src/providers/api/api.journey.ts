export type Journey = {
    id: number;
    title: string;
    points: any;
    created_at: string;
    updated_at: string;
    user_journeys: Array<Journey>;
}