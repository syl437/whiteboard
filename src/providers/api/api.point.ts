import {Touchpoint} from './api.touchpoint';

export type Point = {
    id: number;
    title: string;
    color: string;
    weight: number;
    user_id?: any;
    touchpoint: Touchpoint;
    created_at: string;
    updated_at: string;
}