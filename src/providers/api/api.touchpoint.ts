export type Touchpoint = {
    id: number;
    journey_id: number;
    point_id: number;
    mark: number;
    duration: string;
    lat?: string;
    lng?: string;
    address?: string;
    created_at: string;
    updated_at: string;
}