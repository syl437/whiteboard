import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule, ViewChild} from '@angular/core';
import {AlertController, IonicApp, IonicErrorHandler, IonicModule, Nav} from 'ionic-angular';

import {MyApp} from './app.component';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ApiProvider} from '../providers/api/api';
import {RestangularModule} from 'ngx-restangular';
import {ENV} from '../config/config.dev';
import {AuthProvider} from '../providers/auth/auth';
import {IonicStorageModule} from '@ionic/storage';
import {Camera} from '@ionic-native/camera';
import {FileTransfer} from '@ionic-native/file-transfer';
import {Diagnostic} from '@ionic-native/diagnostic';
import {MediaCapture} from '@ionic-native/media-capture';
import {UtilsProvider} from '../providers/utils/utils';
import {MomentModule} from 'angular2-moment';
import {Geolocation} from '@ionic-native/geolocation';
import {Media} from '@ionic-native/media';
import {File} from '@ionic-native/file';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {Keyboard} from '@ionic-native/keyboard';

export function createTranslateLoader (http: HttpClient){
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


export function RestangularConfigFactory(RestangularProvider, auth: AuthProvider, alertCtrl: AlertController) {
    
    // Setting up the Restangular endpoint - where to send queries
    RestangularProvider.setBaseUrl(ENV.API_ENDPOINT);

    // Setting up attachment of header token
    RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {

        return {
            headers: Object.assign({}, headers, {Authorization: 'Bearer ' + `${auth.token}`})
        };

    });

    // Every time the query is made, it's console.logged.
    // Restangular doesn't support boolean answers from the API, so right now null is returned
    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        if (data) {
            console.log(url, data.data, operation);
        }

        return data.data;

    });

    // Every time when error is received from the server API_ENDPOINT, first it is processed here.
    // IDM_ENDPOINT errors are processed in the functions only (login, refresh-tokens, get-users x 2).
    RestangularProvider.addErrorInterceptor(async (response, subject, responseHandler) => {

        console.log('ErrorInterceptor', response);

        if (response.data && !response.success){
            if (response.data.error && response.data.error.message){
                alertCtrl.create({title: response.data.error.message, buttons: ['OK']}).present();
                console.log("Er1");
            } else {
                alertCtrl.create({title: 'Server error!', buttons: ['OK']}).present();
                console.log("Er2");
            }
        }

    });

}

@NgModule({
    declarations: [
        MyApp,
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp, {iconMode: 'md', mode: 'md'}),
        IonicStorageModule.forRoot(),
        RestangularModule.forRoot([AuthProvider, AlertController], RestangularConfigFactory),
        MomentModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        }),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        ApiProvider,
        AuthProvider,
        Camera,
        FileTransfer,
        Diagnostic,
        MediaCapture,
        UtilsProvider,
        Geolocation,
        Media,
        File,
        Keyboard
    ]
})
export class AppModule {}
