import {Directive, EventEmitter, HostListener, Output} from '@angular/core';
import {AlertController} from "ionic-angular";
import { Camera, CameraOptions } from '@ionic-native/camera';


@Directive({
    selector: '[add-picture]' // Attribute selector
})
export class AddPictureDirective {

    @Output('targetImage')
    targetImage: EventEmitter<object> = new EventEmitter();

    constructor(public alertCtrl: AlertController,
                public camera: Camera) {}

    @HostListener('click', ['$event'])

    onClick(event: Event) {
        this.makeImage();
    }

    makeImage() {

        let options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };

        this.camera.getPicture(options).then((imageData) => {
            console.log(imageData);
            this.targetImage.emit({url: imageData, type: 'image/jpeg'});
        }, (err) => {
            console.log('err', err);
        });

    }
}
