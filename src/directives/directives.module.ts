import {NgModule} from '@angular/core';
import {BackgroundImageDirective} from './background-image/background-image';
import {AddPictureDirective} from './add-picture/add-picture';
import {AddVideoDirective} from './add-video/add-video';

@NgModule({
	declarations: [BackgroundImageDirective,
    AddPictureDirective,
    AddVideoDirective],
	imports: [],
	exports: [BackgroundImageDirective,
    AddPictureDirective,
    AddVideoDirective]
})
export class DirectivesModule {}
