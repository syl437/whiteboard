import { NgModule } from '@angular/core';
import { LoadBarComponent } from './load-bar/load-bar';
@NgModule({
	declarations: [LoadBarComponent],
	imports: [],
	exports: [LoadBarComponent]
})
export class ComponentsModule {}
