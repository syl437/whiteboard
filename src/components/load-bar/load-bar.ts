import {Component, NgZone} from '@angular/core';
import {UtilsProvider} from '../../providers/utils/utils';

@Component({
    selector: 'load-bar',
    templateUrl: 'load-bar.html'
})
export class LoadBarComponent {

    full: number = 0;
    empty: number = 100;

    constructor(public utils: UtilsProvider, public zone: NgZone) {

        this.utils.uploadingProgress$.subscribe(full => {
            this.zone.run(() => {
                this.full = full;
                this.empty = 100 - full;
            });
        });

    }

}
