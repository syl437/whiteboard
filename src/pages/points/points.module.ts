import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PointsPage} from './points';
import {TranslateModule} from '@ngx-translate/core';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    PointsPage,
  ],
  imports: [
    IonicPageModule.forChild(PointsPage),
      TranslateModule,
      ComponentsModule
  ],
})
export class PointsPageModule {}
