import {Component} from '@angular/core';
import {
    AlertController, App, IonicPage, LoadingController, NavController, NavParams, Platform, reorderArray
} from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import {Restangular} from 'ngx-restangular';
import {Journey} from '../../providers/api/api.journey';
import {ReorderIndexes} from 'ionic-angular/components/item/item-reorder';
import {UtilsProvider} from '../../providers/utils/utils';
import {TranslateService} from '@ngx-translate/core';
import {Storage} from '@ionic/storage';

@IonicPage()
@Component({
    selector: 'page-points',
    templateUrl: 'points.html',
})
export class PointsPage {

    user_journey = this.navParams.get('user_journey');
    title: string = this.navParams.get('title');
    points = [];
    journeys: Array<Journey> = [];
    newTouchpoint = '';
    language: string;
    coordinates = {lat: 0, lng: 0};

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public api: ApiProvider,
                public translate: TranslateService,
                public restangular: Restangular,
                public platform: Platform,
                public app: App,
                public utils: UtilsProvider,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public storage: Storage) {
        
        // workaround for hardware back button on Android
        this.platform.registerBackButtonAction(() => {
            this.finish();
        });

        this.api.language$.subscribe(language => this.language = language);

        this.utils.coordinates$.subscribe(coordinates => {this.coordinates = coordinates});
        console.log(this.user_journey , this.title)
    }

    async addTouchpoint (){
        if (this.newTouchpoint !== ''){
            try {
                await this.restangular.all('points').customPOST({title: this.newTouchpoint, journey_id: this.user_journey.journey_id}).toPromise();
                this.newTouchpoint = '';
                await this.api.getJourneys();
            } catch (err){
                console.log(err);
            }
        }
    }

    async finish () {
        try {
            console.log(this.title);
            let alert = this.alertCtrl.create({
                title: this.translate.instant('ALERTS.ENDJOURNEY'),
                message: this.translate.instant('ALERTS.ENDJOURNEY2') + this.title + this.translate.instant('ALERTS.ENDJOURNEY3'),
                buttons: ['OK']
            });
            alert.present();
            await this.utils.checkLocation();
            await this.api.getJourneys();
            this.api.reportTouchpointPeriod('end', this.user_journey.id,  this.coordinates.lat, this.coordinates.lng,null, false);
            this.api.reportJourneyPeriod('end', this.user_journey.id, this.coordinates.lat, this.coordinates.lng);
        } catch (err){
            console.log(err);
        } finally {
            this.navCtrl.setRoot('HomePage');
        }
    }

    goToPoint (point) {
        this.navCtrl.push('PointPage', {user_journey: this.user_journey, point: point});
    }

    async ionViewDidEnter () {
        // TODO: rewrite after debugging
        // await this.api.getUserJourney(3);
        this.getPoints();
    }
    
    async getPoints()
    {
        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();
        console.log("JournyId : " , this.user_journey.id);
        let server = await this.api.getUserJourney(this.user_journey.id);
        loading.dismiss();
        this.api.user_journey$.subscribe(journey => {
            this.user_journey = journey;
            this.api.journeys$.filter(journey => {return journey !== null}).subscribe(async (journeys) => {
                this.journeys = journeys;
                this.points = this.journeys.filter(journey => {return journey.id === this.user_journey.journey_id})[0].points;
                for (let point of this.points){
                    point.isEditPoint = false;
                    if (this.user_journey.touchpoints){
                        for (let touchpoint of this.user_journey.touchpoints){
                            if (touchpoint.point_id === point.id && touchpoint.user_journey_id === this.user_journey.id){
                                point.touchpoint = touchpoint;
                            }
                        }
                    }
                
                }
            
                console.log("Points : " , this.points)
            });
        })
    }

    ionViewDidLeave () {
        for (let point of this.points){
            if (point.touchpoint){
                delete point.touchpoint;
            }
        }
    }

    async reorderItems (event: ReorderIndexes){

        reorderArray(this.points, event);

        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();

        try {
            await this.restangular.one('journeys', this.user_journey.journey_id).customPATCH({'order': this.points.map(p => p.id)}).toPromise();
            await this.api.getJourneys();
        } catch (err){
            console.log(err);
        } finally {
            loading.dismiss();
            console.log(this.points);
        }

    }
    
    async editPoint(point)
    {
        point.isEditPoint = point.isEditPoint == true ? false : true;
        await this.restangular.all('update_points').customPOST({point_id: point.id, point_title: point.title}).toPromise();
    }
    
    
    async deletePoint(point,index)
    {
        console.log(point);
        await this.restangular.all('delete_points').customPOST({point_id: point.id, point_title: point.title}).toPromise();
        this.points.splice(index,1)
        //this.getPoints();
    }
}
