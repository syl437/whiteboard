import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {PointPage} from './point';
import {TranslateModule} from '@ngx-translate/core';
import {ComponentsModule} from '../../components/components.module';
import {DirectivesModule} from '../../directives/directives.module';

@NgModule({
  declarations: [
    PointPage,
  ],
  imports: [
    IonicPageModule.forChild(PointPage),
      TranslateModule,
      ComponentsModule,
      DirectivesModule
  ],
})
export class PointPageModule {}
