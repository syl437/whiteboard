import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, Platform} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthProvider} from '../../providers/auth/auth';
import {Restangular} from 'ngx-restangular';
import {AuthStatuses} from '../../providers/auth/auth.status';
import {ApiProvider} from '../../providers/api/api';
import {Keyboard} from '@ionic-native/keyboard';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    form: FormGroup = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required]
    });
    showFooter: boolean = true;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public fb: FormBuilder,
                public loadingCtrl: LoadingController,
                public auth: AuthProvider,
                public restangular: Restangular,
                public platform: Platform,
                public keyboard: Keyboard,
                public api: ApiProvider) {}

    async submit() {

        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();

        try {

            await this.requestToken(this.form.value.email, this.form.value.password);
            await this.api.getJourneys();

        } catch (err) {
            console.log(err);
        } finally {
            loading.dismiss();
        }

    }

    async requestToken(email: string, password: string): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            try {
                let payload = {email: email, password: password};

                let response = await this.restangular.all('tokens').customPOST(payload).toPromise();

                await this.auth.setToken(response.api_token);
                await this.auth.setId(response.id);
                await this.api.setLanguage(response.language);
                await this.auth.setStatus(AuthStatuses.AUTHENTICATED);

                this.navCtrl.setRoot('HomePage');

                resolve();
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }

}
