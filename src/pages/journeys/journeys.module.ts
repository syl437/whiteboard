import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { JourneysPage } from './journeys';
import {MomentModule} from 'angular2-moment';
import {TranslateModule} from '@ngx-translate/core';
import {ComponentsModule} from '../../components/components.module';

@NgModule({
  declarations: [
    JourneysPage,
  ],
  imports: [
    IonicPageModule.forChild(JourneysPage),
      MomentModule,
      TranslateModule,
      ComponentsModule
  ],
})
export class JourneysPageModule {}
